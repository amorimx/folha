package br.com.cron.folhapagamento.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cron.folhapagamento.model.Item;
import br.com.cron.folhapagamento.repository.ItemRepository;

@Service
public class ItemService {

	@Autowired
	ItemRepository repository;

	public Item saveItem(Item item) {
		return repository.save(item);
	}

	public List<Item> findAll() {
		return (List<Item>) repository.findAll();
	}

	public List<Item> search(Item item) {
		return repository.findByName(item.getName());
	}

	public void deleteItem(Item item) {
		repository.delete(item);
	}

}
