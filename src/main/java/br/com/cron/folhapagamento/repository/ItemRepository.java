package br.com.cron.folhapagamento.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.cron.folhapagamento.model.Item;

public interface ItemRepository extends CrudRepository<Item, Long> {

	List<Item> findByName(String name);

}
