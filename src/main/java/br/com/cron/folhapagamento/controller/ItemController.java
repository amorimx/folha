package br.com.cron.folhapagamento.controller;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.cron.folhapagamento.model.Item;
import br.com.cron.folhapagamento.service.ItemService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;

@Component
public class ItemController implements Initializable {

	@Autowired
	private ItemService itemService;

	@FXML
	private Label item;
	@FXML
	private TextField name;

	@FXML
	private Button novo;
	@FXML
	private Button pesquisar;
	@FXML
	private Button limpar;

	@FXML
	private TableView<Item> tabela;
	@FXML
	private TableColumn<Item, Integer> idCol;
	@FXML
	private TableColumn<Item, String> nameCol;

	@FXML
	private ContextMenu menuTabela;
	@FXML
	private MenuItem editar;
	@FXML
	private MenuItem visualizar;
	@FXML
	private MenuItem excluir;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		initLayout();
		initListeners();
	}

	private ObservableList<Item> listaItems() {
		return FXCollections.observableArrayList(itemService.findAll());
	}

	private ObservableList<Item> pesquisaItems(Item item) {
		return FXCollections.observableArrayList(itemService.search(item));
	}

	public void initLayout() {
		idCol.setCellValueFactory(new PropertyValueFactory<>("idItem"));
		nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));

		tabela.setItems(listaItems());
	}

	public void initListeners() {
		novo.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				TextInputDialog dialog = new TextInputDialog();
				dialog.setTitle("Novo");
				dialog.setHeaderText("Cadastrar Item");
				dialog.setContentText("Nome Item:");

				Optional<String> result = dialog.showAndWait();
				if (result.isPresent()) {
					Item it = new Item();
					it.setName(result.get());
					itemService.saveItem(it);
					tabela.setItems(listaItems());
				}
			}
		});

		pesquisar.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if (name.getText() != null && !name.getText().equals("")) {
					Item item = new Item();
					item.setName(name.getText());
					tabela.setItems(pesquisaItems(item));
				} else {
					tabela.setItems(listaItems());
				}
			}
		});

		limpar.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				name.setText("");
			}
		});

		visualizar.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				Item it = tabela.getSelectionModel().getSelectedItem();
				System.out.println(it);

				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Visualizar");
				alert.setHeaderText(null);
				alert.setContentText("Item: " + it.getName());

				alert.showAndWait();
			}
		});

		editar.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				Item it = tabela.getSelectionModel().getSelectedItem();

				TextInputDialog dialog = new TextInputDialog(it.getName());
				dialog.setTitle("Editar");
				dialog.setHeaderText("Editar Item id: " + it.getIdItem());
				dialog.setContentText("Nome Item:");

				Optional<String> result = dialog.showAndWait();
				if (result.isPresent()) {
					it.setName(result.get());
					itemService.saveItem(it);
					tabela.setItems(listaItems());
				}
			}
		});

		excluir.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("Confirmação");
				alert.setContentText("Deseja excluir este item?");

				Optional<ButtonType> result = alert.showAndWait();
				if (result.get() == ButtonType.OK) {
					Item it = tabela.getSelectionModel().getSelectedItem();

					itemService.deleteItem(it);
					tabela.setItems(listaItems());
					tabela.getItems().remove(it);

				}

			}
		});
	}

}
