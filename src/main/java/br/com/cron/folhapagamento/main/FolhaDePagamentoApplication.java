package br.com.cron.folhapagamento.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

@SpringBootApplication(scanBasePackages = { "br.com.cron.folhapagamento.service",
		"br.com.cron.folhapagamento.controller", "br.com.cron.folhapagamento.main" })
@EntityScan(basePackages = { "br.com.cron.folhapagamento.model" })
@EnableJpaRepositories(basePackages = { "br.com.cron.folhapagamento.repository" })
public class FolhaDePagamentoApplication extends Application {

	private static final Logger log = LoggerFactory.getLogger(FolhaDePagamentoApplication.class);

	private ConfigurableApplicationContext context;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void init() throws Exception {
		super.init();
		SpringApplicationBuilder builder = new SpringApplicationBuilder(FolhaDePagamentoApplication.class);
		context = builder.run(getParameters().getRaw().toArray(new String[0]));

	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		log.info("Starting...");

		FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/Item.fxml"));
		loader.setControllerFactory(context::getBean);
		Parent root = loader.load();
		Scene scene = new Scene(root, 600, 400);
		primaryStage.setScene(scene);
		primaryStage.setTitle("TableView App");
		primaryStage.show();
	}
}
